package com.tdd.bootcamp2;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Scanner currScanner = new Scanner(System.in);
        Player player1 = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Player player2 = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Game game = new Game(player1, player2, System.out, 3, new Machine());
        game.play();
    }
}
