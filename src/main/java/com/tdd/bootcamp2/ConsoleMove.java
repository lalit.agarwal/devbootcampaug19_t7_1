package com.tdd.bootcamp2;

import java.util.Scanner;

public class ConsoleMove implements MovableBehaviour {

    Scanner reader;

    public ConsoleMove(Scanner reader) {
        this.reader = reader;
    }


    @Override
    public MoveEnum nextMove() {
        return reader.nextLine().equals("1") ? MoveEnum.COOPERATE : MoveEnum.CHEAT;
    }
}
