package com.tdd.bootcamp2;

import java.util.Observable;
import java.util.Observer;

public class CopyCatBehaviour implements MovableBehaviour, Observer {

    private MoveEnum opponentMove = MoveEnum.COOPERATE;
    private MoveEnum previousMove;
    @Override
    public MoveEnum nextMove() {
        return opponentMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        MoveEnum[] moves = (MoveEnum[]) arg;
        opponentMove = opponentMove == moves[0] ? moves[1] : moves[0];
    }
}
