package com.tdd.bootcamp2;

import java.io.PrintStream;
import java.util.Observable;

public class Game extends Observable {

    private final Player player1;
    private final Player player2;
    PrintStream resultPrintStream;
    int noOfRounds;
    Machine machine;

    public Game(Player player1, Player player2, PrintStream resultPrintStream, int noOfRounds, Machine machine) {
        this.player1 = player1;
        this.player2 = player2;
        this.resultPrintStream = resultPrintStream;
        this.noOfRounds = noOfRounds;
        this.machine = machine;
    }

    public void play() {
        for (int i = 0; i < noOfRounds; i++) {
            MoveEnum player1Move = this.player1.getLatestMove();
            MoveEnum player2Move = this.player2.getLatestMove();
            int[] scoreArray = machine.calculateScore(player1Move, player2Move);
            this.player1.addScore(scoreArray[0]);
            this.player2.addScore(scoreArray[1]);
            String resultString = String.format("Score is %d %d", this.player1.score(), this.player2.score());
            resultPrintStream.println(resultString);
            setChanged();
            notifyObservers(new MoveEnum[]{player1Move,player2Move});
        }
    }
}
