package com.tdd.bootcamp2;

import java.util.Observable;
import java.util.Observer;

import static com.tdd.bootcamp2.MoveEnum.CHEAT;
import static com.tdd.bootcamp2.MoveEnum.COOPERATE;

public class GrudgerBehaviour implements MovableBehaviour, Observer {

    private MoveEnum grudgerMove = COOPERATE;

    @Override
    public MoveEnum nextMove() {
        return grudgerMove;
    }

    @Override
    public void update(Observable game, Object arg) {
        MoveEnum[] moves = (MoveEnum[]) arg;
        if(moves[0] == CHEAT || moves[1] == CHEAT) {
            grudgerMove = CHEAT;
            game.deleteObserver(this);
        }
    }
}
