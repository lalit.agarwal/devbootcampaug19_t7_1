package com.tdd.bootcamp2;

public class Machine {
    public int[]  calculateScore(MoveEnum player1Move, MoveEnum player2Move) {
        if (player1Move == MoveEnum.COOPERATE && player2Move == MoveEnum.COOPERATE){
            return new int[]{2,2};
        } else if (player1Move == MoveEnum.CHEAT && player2Move == MoveEnum.CHEAT){
            return new int[]{0,0};
        } else if( player1Move == MoveEnum.COOPERATE && player2Move == MoveEnum.CHEAT){
            return  new int[]{-1, 3};
        } else if( player1Move == MoveEnum.CHEAT && player2Move == MoveEnum.COOPERATE){
            return  new int[]{3, -1};
        }
        return new int[]{0, 0};
    }
}
