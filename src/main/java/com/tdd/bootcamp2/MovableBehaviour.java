package com.tdd.bootcamp2;

public interface MovableBehaviour {

    MovableBehaviour COOPERATE_BEHAVIOUR = () -> MoveEnum.COOPERATE;
    MovableBehaviour CHEAT_BEHAVIOUR = () -> MoveEnum.CHEAT;
    MoveEnum nextMove();
}
