package com.tdd.bootcamp2;

import java.util.Observable;
import java.util.Observer;

import static com.tdd.bootcamp2.MoveEnum.CHEAT;

public class Player {

    private int scoreKeeper;
    private MovableBehaviour movableBehaviour;

    public Player(MovableBehaviour movableBehaviour) {
        this.movableBehaviour = movableBehaviour;
        this.scoreKeeper = 0;
    }

    public void addScore(int score) {
        this.scoreKeeper += score;
    }

    public MoveEnum getLatestMove() {
        return movableBehaviour.nextMove();
    }

    public int score() {
        return scoreKeeper;
    }

    public MovableBehaviour getMovableBehaviour() {
        return this.movableBehaviour;
    }
}
