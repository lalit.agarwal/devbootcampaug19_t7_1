package com.tdd.bootcamp2;

import org.junit.Ignore;
import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameIntegrationTest {

    @Test
    public void shouldPlayGameBetweenCheatPlayerAndCooperatePlayer() {
        PrintStream console = mock(PrintStream.class);
        Player cheatPlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Player cooperatePlayer = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Game game = new Game(cheatPlayer, cooperatePlayer, console, 5, new Machine());
        game.play();
        verify(console).println("Score is 3 -1");
        verify(console).println("Score is 6 -2");
        verify(console).println("Score is 9 -3");
        verify(console).println("Score is 12 -4");
        verify(console).println("Score is 15 -5");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatPlayerAndCooperatePlayer() {
        PrintStream console = mock(PrintStream.class);
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Player cooperatePlayer = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Game game = new Game(copyCatPlayer, cooperatePlayer, console, 5, new Machine());
        game.addObserver(copyCatBehaviour);
        game.play();
        verify(console).println("Score is 2 2");
        verify(console).println("Score is 4 4");
        verify(console).println("Score is 6 6");
        verify(console).println("Score is 8 8");
        verify(console).println("Score is 10 10");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatPlayerAndCheatPlayer() {
        PrintStream console = mock(PrintStream.class);
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Player cheatPlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Game game = new Game(copyCatPlayer, cheatPlayer, console, 5, new Machine());
        game.addObserver(copyCatBehaviour);
        game.play();
        verify(console,times(5)).println("Score is -1 3");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatPlayerAndCopyCatPlayer() {
        PrintStream console = mock(PrintStream.class);
        CopyCatBehaviour copyCatBehaviour1 = new CopyCatBehaviour();
        Player copyCatPlayer1 = new Player(copyCatBehaviour1);
        CopyCatBehaviour copyCatBehaviour2 = new CopyCatBehaviour();
        Player copyCatPlayer2 = new Player(copyCatBehaviour2);
        Game game = new Game(copyCatPlayer1, copyCatPlayer2, console, 5, new Machine());
        game.addObserver(copyCatBehaviour1);
        game.addObserver(copyCatBehaviour2);
        game.play();
        verify(console).println("Score is 2 2");
        verify(console).println("Score is 4 4");
        verify(console).println("Score is 6 6");
        verify(console).println("Score is 8 8");
        verify(console).println("Score is 10 10");
    }

    @Test
    public void shouldPlayGameBetweenCooperatePlayerAndCopyCatPlayer() {
        PrintStream console = mock(PrintStream.class);
        Player cooperatePlayer = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Game game = new Game(cooperatePlayer, copyCatPlayer, console, 5, new Machine());
        game.addObserver(copyCatBehaviour);
        game.play();
        verify(console).println("Score is 2 2");
        verify(console).println("Score is 4 4");
        verify(console).println("Score is 6 6");
        verify(console).println("Score is 8 8");
        verify(console).println("Score is 10 10");
    }

    @Test
    public void shouldPlayGameBetweenCheatPlayerAndCopyCatPlayer() {
        PrintStream console = mock(PrintStream.class);
        Player cheatPlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Game game = new Game(cheatPlayer, copyCatPlayer, console, 5, new Machine());
        game.addObserver(copyCatBehaviour);
        game.play();
        verify(console,times(5)).println("Score is 3 -1");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatAndGrudgerPlayer() {
        PrintStream console = mock(PrintStream.class);
        GrudgerBehaviour grudgerBehaviour = new GrudgerBehaviour();
        Player grudgerPlayer = new Player(grudgerBehaviour);
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Game game = new Game(copyCatPlayer, grudgerPlayer, console, 5, new Machine());
        game.addObserver(copyCatBehaviour);
        game.addObserver(grudgerBehaviour);
        game.play();
        verify(console).println("Score is 2 2");
        verify(console).println("Score is 4 4");
        verify(console).println("Score is 6 6");
        verify(console).println("Score is 8 8");
        verify(console).println("Score is 10 10");
    }

    @Test
    public void shouldPlayGameBetweenCheatAndGrudgerPlayer() {
        PrintStream console = mock(PrintStream.class);
        GrudgerBehaviour grudgerBehaviour = new GrudgerBehaviour();
        Player grudgerPlayer = new Player(grudgerBehaviour);
        Player cheatPlayer = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Game game = new Game(cheatPlayer, grudgerPlayer, console, 5, new Machine());
        game.addObserver(grudgerBehaviour);
        game.play();
        verify(console,times(5)).println("Score is 3 -1");
    }
}
