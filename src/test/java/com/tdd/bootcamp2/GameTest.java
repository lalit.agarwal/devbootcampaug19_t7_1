package com.tdd.bootcamp2;

import org.junit.Ignore;
import org.junit.Test;

import java.io.PrintStream;

import static com.tdd.bootcamp2.MoveEnum.CHEAT;
import static com.tdd.bootcamp2.MoveEnum.COOPERATE;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldReturnScoreTwoAndTwoWhenBothCooperate() {
        PrintStream resultPrintStream = mock(PrintStream.class);
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        Game game = new Game(player1, player2, resultPrintStream, 1, machine);

        when(player1.getLatestMove()).thenReturn(COOPERATE);
        when(player2.getLatestMove()).thenReturn(COOPERATE);
        when(player1.score()).thenReturn(2);
        when(player2.score()).thenReturn(2);
        when(machine.calculateScore(COOPERATE, COOPERATE)).thenReturn(new int[]{2, 2});

        game.play();

        verify(player1, times(1)).getLatestMove();
        verify(player2, times(1)).getLatestMove();
        verify(player1, times(1)).addScore(2);
        verify(player2, times(1)).addScore(2);
        verify(resultPrintStream).println("Score is 2 2");
    }

    @Test
    public void shouldReturnScoreMinusOneAndThreeWhenPlayer1CooperateAndPlayer2Cheat() {
        PrintStream resultPrintStream = mock(PrintStream.class);
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);

        Game game = new Game(player1, player2, resultPrintStream, 1, machine);

        when(player1.getLatestMove()).thenReturn(COOPERATE);
        when(player2.getLatestMove()).thenReturn(CHEAT);
        when(player1.score()).thenReturn(-1);
        when(player2.score()).thenReturn(3);
        when(machine.calculateScore(COOPERATE, CHEAT)).thenReturn(new int[]{-1, 3});

        game.play();

        verify(player1, times(1)).getLatestMove();
        verify(player2, times(1)).getLatestMove();
        verify(player1, times(1)).addScore(-1);
        verify(player2, times(1)).addScore(3);
        verify(resultPrintStream).println("Score is -1 3");
    }

    @Test
    public void gamePlayWithMultipleRounds() {
        PrintStream resultPrintStream = mock(PrintStream.class);
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);

        Game game = new Game(player1, player2, resultPrintStream, 2, machine);

        when(player1.getLatestMove()).thenReturn(COOPERATE).thenReturn(COOPERATE);
        when(player2.getLatestMove()).thenReturn(CHEAT).thenReturn(CHEAT);
        when(player1.score()).thenReturn(-1).thenReturn(-2);
        when(player2.score()).thenReturn(3).thenReturn(6);
        when(machine.calculateScore(COOPERATE, CHEAT)).thenReturn(new int[]{-1, 3}).thenReturn(new int[]{-1, 3});

        game.play();

        verify(player1, times(2)).getLatestMove();
        verify(player2, times(2)).getLatestMove();
        verify(player1, times(2)).addScore(-1);
        verify(player2, times(2)).addScore(3);
        verify(resultPrintStream).println("Score is -1 3");
        verify(resultPrintStream).println("Score is -2 6");
    }
}
