package com.tdd.bootcamp2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class MoveReaderTest {

    private ConsoleMove moveReader;

    @Test
    public void shouldReturnCooperationWhenInputToScannerisOne() {
        Scanner mockScanner = new Scanner("1");
        moveReader = new ConsoleMove(mockScanner);
        Assert.assertEquals(MoveEnum.COOPERATE, this.moveReader.nextMove());
    }

    @Test
    public void shouldReturnCheatWhenInputToScannerisOtherThanOne() {
        Scanner mockScanner = new Scanner("0");
        moveReader = new ConsoleMove(mockScanner);
        Assert.assertEquals(MoveEnum.CHEAT, this.moveReader.nextMove());
    }
}
