package com.tdd.bootcamp2;

import com.tdd.bootcamp2.MovableBehaviour;
import com.tdd.bootcamp2.MoveEnum;
import com.tdd.bootcamp2.Player;
import org.junit.Assert;
import org.junit.Test;

import static com.tdd.bootcamp2.MoveEnum.CHEAT;
import static com.tdd.bootcamp2.MoveEnum.COOPERATE;

public class PlayerTest {
    private MovableBehaviour inputReader = () -> CHEAT;

    @Test
    public void shouldSetScore(){
        Player player = new Player(inputReader);
        player.addScore(1);
        Assert.assertEquals(1, player.score());
        player.addScore(2);
        Assert.assertEquals( 3, player.score());
    }

    @Test
    public void shouldMakeCHEATMove(){
        Player player = new Player(()-> CHEAT);
        Assert.assertEquals(CHEAT, player.getLatestMove());
    }

    @Test
    public void shouldMakeCOOPERATEMove(){
        Player player = new Player(()-> COOPERATE);
        Assert.assertEquals(COOPERATE, player.getLatestMove());
    }

    @Test
    public void shouldReturnAlwaysCheatPlayer(){
        Player player = new Player(MovableBehaviour.CHEAT_BEHAVIOUR);
        Assert.assertEquals(CHEAT, player.getLatestMove());
    }

    @Test
    public void shouldReturnAlwaysCooperatePlayer(){
        Player player = new Player(MovableBehaviour.COOPERATE_BEHAVIOUR);
        Assert.assertEquals(COOPERATE, player.getLatestMove());
    }
}
